"use strict";
class LibraryLoader {
	constructor(libraryStructure) {
		this.libraryStructure = libraryStructure || {
			directory: "source/",
			libraries: []
		};	
		return this;
	}
	
	load(callback) {
		if(this.libraryStructure.directory && this.libraryStructure.libraries) {
			var loadPromises = [];
			for(var i = 0; i < this.libraryStructure.libraries.length; i++)
				loadPromises.push(this.loadSingle(this.libraryStructure.libraries[i]));

			return Promise.all(loadPromises).then(() => {
				return "All scripts loaded";
			});
		}
		else {
			return Promise.reject(new Error("No scripts to load."));
		}
	}
	
	loadSingle(libraryName) {
		libraryName = libraryName || "";
		return new Promise((resolve, reject) => {
			var newScript = document.createElement("script");
			var scriptUrl = this.libraryStructure.directory + libraryName;
			newScript.setAttribute("src", scriptUrl);
			document.head.appendChild(newScript);
			
			newScript.onload = () => {
				resolve(scriptUrl);
			}
			
			newScript.error = () => {
				reject(new Error(this.statusText));
			}
		});
	}
}
