class Tasks {
	constructor(data) {
		data = data || {};	
		this.url = data.url || "main.php";
		this.id = data.id || 0;
		this.dom = data.dom || {};
		
		this.tasks = [];
		
		this.createToolBar();		
		this.getTasks();
		
		return this;
	}
	
	getTasks() {
		Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
		Common.loadData(this.url + "?list=task&area_id=" + this.id).then((response) => {
			Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
			try {
				let tasks = JSON.parse(response);
				for(let key in tasks) {
					if(key === "tasks") {
						let newTasks = [];
						
						for(let taskKey in tasks[key]) {
							let newTask = this.createTask(tasks[key][taskKey]);
							newTasks.push(newTask);
						}
						
						for(let i = 0; i < this.tasks.length; i++) {
							let exists = false;
							for(let j = 0; j < newTasks.length; j++) {
								if(this.tasks[i].data.id === newTasks[j].data.id) {
									exists = newTasks[j];
								}
							}
							
							if(!exists) {
								this.tasks[i].dom.parentNode.removeChild(this.tasks[i].dom);
								this.tasks.splice(i, 1);
							}
							else {
								let oldTask = this.tasks[i].dom;
								let parent = oldTask.parentNode;
								
								this.tasks[i] = exists;
								if(parent) {
									parent.replaceChild(exists.dom, oldTask);
								}
							}
						}
						
						for(let i = 0; i < newTasks.length; i++) {
							let exists = false;
							for(let j = 0; j < this.tasks.length; j++) {
								if(newTasks[i].data.id === this.tasks[j].data.id) {
									exists = true;
									break;
								}
							}
							if(!exists) {
								this.tasks.push(newTasks[i]);
							}
						}
					}
				}
				this.listTasks();
			}
			catch(e) {
				console.error("Parsing areas data failed", e.message);
			}
		}, function(error) {
			Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
			console.error("Failed!", error);
		});
	}
	
	createTaskItemEditableWrapper(type, data, labelText) {
		let wrapper = document.createElement("div");
		wrapper.classList.add("editable", type);
		
		if(labelText) {
			let label = document.createElement("span");
			label.innerText = labelText;
			wrapper.appendChild(label);
		}
		
		let editable = document.createElement("span");
		editable.contentEditable = true;
		editable.classList.add("text");
		editable.innerText = data || "";
		wrapper.appendChild(editable);
		
		return {
			wrapper: wrapper,
			editable: editable
		};
	}
	
	createTask(data) {
		data = data || {};
				
		let taskWrapper = document.createElement("div");
		taskWrapper.classList.add("task-wrapper");
		
		let task = document.createElement("div");
		task.classList.add("task");
		taskWrapper.appendChild(task);
		
		let deleteTask = document.createElement("button");
		deleteTask.classList.add("delete");
		deleteTask.title = "Delete this task";
		deleteTask.innerText = "delete";
		task.appendChild(deleteTask);
		
		let saveTask = document.createElement("button");
		saveTask.classList.add("save");
		saveTask.title = "Save this task";
		saveTask.innerText = "save";
		task.appendChild(saveTask);
		
		let id = document.createElement("div");
		id.classList.add("id");
		id.innerText = data.id || "";
		task.appendChild(id);
		
		let nameItem = this.createTaskItemEditableWrapper("name", data.name);
		let name = nameItem.editable;
		task.appendChild(nameItem.wrapper);
		
		let descriptionItem = this.createTaskItemEditableWrapper("description", data.description, "Description:");
		let description = descriptionItem.editable;
		task.appendChild(descriptionItem.wrapper);
		
		let contentItem = this.createTaskItemEditableWrapper("content", data.content, "Content:");
		let content = contentItem.editable;
		task.appendChild(contentItem.wrapper);
		
		let extensionItem = this.createTaskItemEditableWrapper("extension", data.extension, "Extension:");
		let extension = extensionItem.editable;
		task.appendChild(extensionItem.wrapper);
		
		deleteTask.addEventListener("click", () => {
			if(confirm("Are you sure you want to delete this task?")) {
				Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
				Common.pushPostData(this.url + "?delete=task&area_id=" + this.id + "&id=" + data.id).then((response) => {
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
					this.getTasks();
				}, function(error) {
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
					console.error("Failed!", error);
				});
			}
		});
		
		saveTask.addEventListener("click", () => {
			let saveData = new FormData();
			saveData.append("id", data.id);
			saveData.append("name", name.innerText);
			saveData.append("description", description.innerText);
			saveData.append("content", content.innerText);
			saveData.append("extension", extension.innerText);
			
			Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
			Common.pushPostData(this.url + "?update=task&area_id=" + this.id + "&id=" + data.id, saveData).then((response) => {
				Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
				this.getTasks();
			}, function(error) {
				Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
				console.error("Failed!", error);
			});
		});
				
		return {
			dom: taskWrapper,
			data: data
		};
	}
	
	listTasks() {
		if(this.dom.tasks) {
			if(!this.taskWrapper) {
				this.taskWrapper = document.createElement("div");
				this.taskWrapper.classList.add("flex-task-wrapper");
				this.dom.tasks.appendChild(this.taskWrapper);
			}
			
			while(this.taskWrapper.firstChild) {
				this.taskWrapper.removeChild(this.taskWrapper.firstChild);
			}
			
			for(let i = 0; i < this.tasks.length; i++) {
				this.taskWrapper.appendChild(this.tasks[i].dom);
			}
		}
	}
	
	newTaskWrapper() {
		if(this.dom.commonSpaceArea) {
			if(this.dom.commonSpaceArea.getElementsByClassName("new-task").length <= 0) {
				let newWrapper = document.createElement("div");
				newWrapper.classList.add("new-task");
				
				let inputName = document.createElement("input");
				newWrapper.appendChild(inputName);
				
				let inputDescription = document.createElement("input");
				newWrapper.appendChild(inputDescription);
				
				let textareaContent = document.createElement("textarea");
				newWrapper.appendChild(textareaContent);
				
				let inputExtension = document.createElement("input");
				newWrapper.appendChild(inputExtension);
				
				let newButton = document.createElement("button");
				newButton.innerText = "new";
				newWrapper.appendChild(newButton);
				
				this.dom.commonSpaceArea.appendChild(newWrapper);				
				
				newButton.addEventListener("click", () => {
					if(inputName.value) {
						let saveData = new FormData();
						saveData.append("name", inputName.value);
						saveData.append("description", inputDescription.value);
						saveData.append("content", textareaContent.value);
						saveData.append("extension", inputExtension.value);
						
						Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
						Common.pushPostData(this.url + "?new=task&area_id=" + this.id, saveData).then((response) => {
							Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
							console.log(response);
							this.getTasks();
						}, function(error) {
							Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
							console.error("Failed!", error);
						});
					}
				});
			}
		}		
	}
	
	createToolBar() {
		if(this.dom.tasks) {
			let toolBar = document.createElement("div");
			toolBar.classList.add("toolbar");
			this.dom.tasks.appendChild(toolBar);
			
			let newTask = document.createElement("button");
			newTask.classList.add("action", "new-task");
			newTask.title = "Create new task under this area";
			newTask.innerHTML = "New Task";
			toolBar.appendChild(newTask);
			
			newTask.addEventListener("click", () => {
				if(this.taskWrapper) {
					let newTask = this.createTask({});
					this.tasks.push(newTask);
					this.taskWrapper.appendChild(newTask.dom);
				}
			});
			
			let refreshTasks = document.createElement("button");
			refreshTasks.classList.add("action", "refresh-task");
			refreshTasks.title = "Refresh task's list";
			refreshTasks.innerHTML = "Reload tasks";
			toolBar.appendChild(refreshTasks);
			
			refreshTasks.addEventListener("click", () => {
				if(this.taskWrapper) {
					this.getTasks();
				}
			});
		}
	}
}
