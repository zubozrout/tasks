<?php
class BasicTask {  
	private $data = null;
	private $connection = null;
	private $id = 0;
	private $name = null;
	  
	public function __construct(TaskData $data, $connection = null) {
		if(!isset($data)) {
			return false;
		}
		
		$this->data = $data;
		$this->connection = $connection;
		$this->id = (int)$this->data->id;
		$this->area_id = (int)$this->data->area_id;
		$this->name = (String)$this->data->name;
	}

    public function printTask() {
        echo "id: " . $this->id . "\n";
        echo "name: " . $this->name . "\n";
        echo "description: " . $this->data->description . "\n";
        echo "content: " . $this->data->content . "\n";
    }
    
    public function getInsertSql($skipID = true) {
		if($this->connection) {
			$names = [];
			$values = [];
			foreach($this->data as $key => $value) {
				if($skipID && $key === "id") {
					break;
				}
				
				array_push($names, $key);
				array_push($values, "\"" . htmlspecialchars($value) . "\"");
			}
			return "(" . implode(", ", $names) . ") VALUES (" . implode(", ", $values) . ")";
		}
		return false;
	}
    
    public function getData() {
		return $this->data;
	}
	
	public function setId($id = 0) {
		$this->id = $id;
		$this->data->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getAreaId() {
		return $this->area_id;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function toJSON() {
		$obj = new stdClass();
		$obj->id = htmlspecialchars_decode($this->id);
		$obj->name = htmlspecialchars_decode($this->name);
		$obj->description = htmlspecialchars_decode($this->data->description);
		$obj->content = htmlspecialchars_decode($this->data->content);
		$obj->extension = htmlspecialchars_decode($this->data->extension);
		return json_encode($obj);
	}
}
?>
