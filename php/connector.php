<?php
	
class ActionController {
	private $taskApp = null;
	
	public function __construct(TaskApp $taskApp) {
		$this->taskApp = $taskApp;
		
		echo "{\"process\": ";
		if(isset($_GET["list"])) {
			$this->mainPicker("list", $_GET["list"]);
		}
		else if(isset($_GET["new"])) {
			$this->mainPicker("new", $_GET["new"]);
		}
		else if(isset($_GET["update"])) {
			$this->mainPicker("update", $_GET["update"]);
		}
		else if(isset($_GET["delete"])) {
			$this->mainPicker("delete", $_GET["delete"]);
		}
		echo "}";
	}
	
	private function mainPicker($action, $item = null) {
		echo "[\"$action\", ";
		switch($action) {
			case "list":
				return $this->listItems($item);
			case "new":
				return $this->create($item);
			case "update":
				if(isset($_GET["id"])) {
					return $this->update($item, $_GET["id"]);
				}
				return false;
			case "delete":
				if(isset($_GET["id"])) {
					return $this->delete($item, $_GET["id"]);
				}
			default:
				echo "default";
		}
	}
	
	private function listItems($item) {
		switch($item) {
			case "area":
				echo "\"list areas\"], \"areas\": ";
				return $this->taskApp->listAllAreas();
			case "task":
				echo "\"list tasks\"], \"tasks\": ";
				if(isset($_GET["area_id"])) {
					return $this->taskApp->listAllTask($_GET["area_id"]);
				}
				return false;
			default:
			
		}
	}
	
	private function create($item) {
		switch($item) {
			case "area":
				echo "\"create area\", ";
				return $this->createArea();
			case "task":
				echo "\"create task\", ";
				return $this->createTask();
			default:
			
		}
	}
	
	private function update($item, $id) {
		switch($item) {
			case "area":
				echo "\"update area\", ";
				return $this->updateArea($id);
			case "task":
				echo "\"update task\", ";
				return $this->updateTask($id);
			default:
			
		}
	}
	
	private function delete($item, $id) {
		switch($item) {
			case "area":
				echo "\"delete area\", ";
				return $this->deleteArea($id);
			case "task":
				echo "\"delete task\", ";
				return $this->deleteTask($id);
			default:
			
		}
	}
	
	private function createArea() {
		echo "\"new area\"]";
		if(isset($_GET["name"])) {
			return $this->taskApp->createArea($_GET["name"]);
		}
		return false;
	}
	
	private function updateArea($id) {
		echo "\"update area $id\"]";
		if(isset($_GET["name"])) {
			return $this->taskApp->updateArea($id, $_GET["name"]);
		}
		return false;
	}
	
	private function deleteArea($id) {
		echo "\"delete area $id\"]";
		return $this->taskApp->deleteArea($id);
	}
	
	private function createTask() {
		echo "\"new task\"]";
		if(isset($_GET["area_id"])) {
			require_once("taskdata.php");
			$data = (object) $_POST;
			$data->area_id = $_GET["area_id"];
			var_dump($data);
			return $this->taskApp->createTask(new TaskData($data));
		}
		return false;
	}
	
	private function updateTask($id) {
		echo "\"update task $id\"]";
		if(isset($_GET["area_id"])) {
			require_once("taskdata.php");
			$data = (object) $_POST;
			$data->area_id = $_GET["area_id"];
			var_dump($data);
			return $this->taskApp->updateTask($id, new TaskData($data));
		}
		return false;
	}
	
	private function deleteTask($id) {
		echo "\"delete task $id\"]";
		if(isset($_GET["area_id"])) {
			return $this->taskApp->deleteTask($_GET["area_id"], $id);
		}
		return false;
	}
	
	/*

	if(isset($_GET["deleteid"])) {
		$taskApp->joinedRemoveTask($_GET["deleteid"]);
	}
	
	if(isset($_GET["newtask"])) {
		$newTask = $taskApp->postToTask();
		$id = $taskApp->saveTaskToDB($newTask);
		if($id >= 0) {
			$newTask->setId($id);
			$taskApp->allTasksToJSON($newTask);
		}
	}
	
	if(isset($_GET["updatetask"])) {
		$task = $taskApp->postToTask(true);
		$taskApp->allTasksToJSON($task);
		$taskApp->saveTaskToDB($task);
	}
	
	if(isset($_GET["delete"])) {
		if(isset($_POST) && isset($_POST["id"])) {
			$taskApp->joinedRemoveTask($_POST["id"]);
		}
	}
	
	if(isset($_GET["alltojson"])) {
		$taskApp->allTasksToJSON();
	}
	
	if(isset($_GET["listcategories"])) {
		$taskApp->allTasksToJSON();
	}
	
	if(isset($_GET["listareas"])) {
		$taskApp->listAreas();
	}
	
	if(isset($_GET["savealltasks"])) {
		$taskApp->saveAllTasks();
	}
	
	*/
}

?>
