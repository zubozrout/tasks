<?php

require_once("areas.php");
require_once("taskdata.php");

class TaskApp {
	private $areas = [];
	private $areasTableName = "taskareas";
	private $taskTableName = "tasks";
	
	public function __construct($loadDB = true) {
		require_once("task.php");
		require_once("pdoconnection.php");
		
		$this->dbOperations = new DBOperations();
		$this->createAreasTableIfNotExists();	
		$this->createTaskTableIfNotExists();	
		if($loadDB) {
			$this->loadDBAreas();
		}
	}
	
	private function createAreasTableIfNotExists() {
		$this->dbOperations->createTable($this->areasTableName, "id INT(4) UNSIGNED AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT NULL");
	}
	
	private function createTaskTableIfNotExists() {
		$structure = TaskData::DBstructure();
		$structureLength = sizeof($structure);
		
		$initQuery = "";
		$index = 0;
		foreach($structure as $key => $attribute) {
			if($key == "PRIMARY KEY") {
				$initQuery .= "$key ($attribute)";
				break;
			}
			
			$initQuery .= "$key $attribute";
			
			if($index < $structureLength - 1) {
				$initQuery .= ", ";
			}
			$index++;
		}
		$this->dbOperations->createTable($this->taskTableName, $initQuery);
	}
    
    private function loadDBAreas() {
        $response = $this->dbOperations->query("SELECT * FROM " . $this->areasTableName);
        if($response) {
			foreach($response as $area) {
				$this->registerNewArea($area);
			}
		}
    }
    
    private function getAreaById($id) {
		$id = (int)$id;
		
		foreach($this->areas as $area) {
			if($id === $area->getId()) {
				return $area;
			}
		}
		return false;
    }
    
    public function registerNewTask($rawData = null) {
		return $this->newTask($this->objectToTaskData($rawData));
    }
    
    public function createTask(TaskData $taskData) {
		$area = $this->getAreaById($taskData->getAreaId());
		if($area) {
			$newTask = new BasicTask($taskData, $this->dbOperations->database->getConnection());
			$area->registerTask($newTask);
			return $newTask;
		}
        return false;
    }
    
    public function updateTask($id, TaskData $taskData) {
		$id = (int)$id;
		$area = $this->getAreaById($taskData->getAreaId());
		
		if($area) {
			$newTask = new BasicTask($taskData, $this->dbOperations->database->getConnection());
			$area->updateTask($id, $newTask);
			return $newTask;
		}
        return false;
    }
    
    public function registerNewArea($area = null) {
		if($area) {
			array_push($this->areas, new Areas($area, $this->dbOperations));
		}
    }
	
	public function saveAreaToDB($category = null) {
		$category = (String)$category;
		
		if($category) {
			$data = Array("name" => $category);
			return $this->dbOperations->insertOrUpdate("REPLACE INTO " . $this->areasTableName . " (name) VALUES (:name)", $data);
		}
	}
	
	public function createArea($name) {
		$name = (String)$name;
		
		if($name) {
			$data = Array("name" => $name);
			return $this->dbOperations->insertOrUpdate("INSERT INTO " . $this->areasTableName . " (name) VALUES (:name)", $data);
			
		}
	}
	
	public function updateArea($id, $name) {
		$id = (int)$id;
		$name = (String)$name;
		
		if($id >= 0 && $name) {
			$data = Array("id" => $id, "name" => $name);
			return $this->dbOperations->insertOrUpdate("REPLACE INTO " . $this->areasTableName . " (id, name) VALUES (:id, :name)", $data);
		}
	}
	
	public function deleteArea($id) {
		$id = (int)$id;
		$area = $this->getAreaById($id);
		
		if($area) {
			$area->deleteAllTasks();
			return $this->dbOperations->insertOrUpdate("DELETE FROM " . $this->areasTableName . " WHERE id = (:id)", array("id" => $id));
		}
	}
	
	public function deleteTask($area_id, $id) {
		$area_id = (int)$area_id;
		$id = (int)$id;
		
		$area = $this->getAreaById($area_id);
		if($area) {
			return $area->deleteTask($id);
		}
		return false;
	}
	
	public function listAllAreas() {		
		echo "[";
		for($i = 0; $i < sizeof($this->areas); $i++) {
			$area = $this->areas[$i];
			echo "{\"id\": " . $area->getId() . ", \"name\": \"" . $area->getName() . "\"}";
			if($i < sizeof($this->areas) - 1) {
				echo ",";
			}
		}
		echo "]";
	}
	
	public function listAllTask($area_id) {
		$area_id = (int)$area_id;
		$area = $this->getAreaById($area_id);
		
		echo "[";
		if($area) {
			for($i = 0; $i < sizeof($area->getTasks()); $i++) {
				$task = $area->getTasks()[$i];
				echo $task->toJSON();
				if($i < sizeof($area->getTasks()) - 1) {
					echo ",";
				}
			}
		}
		echo "]";
	}
}
?>
