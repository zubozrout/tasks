<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>Tasks</title>
	<link rel="stylesheet" type="text/css" href="tasks.css">
	<script src="libraryloader.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<header>
		<img src="icons/loading.gif" alt="loader" class="loader" id="loadingicon">
		<h1>Tasks</h1>
	</header>
	<main>
		<section class="areas">
			<div id="areaSelector"></div>
			<div id="areas"></div>
		</section>		
		
		<section class="tasks">
			<div class="wrapper full">
				<div id="tasks" class="tasks"></div>
			</div>
		</section>
		
		<section class="newtask">
			<div class="wrapper">
				<form id="taskform"></form>
			</div>
		</section>
		
		<div id="common-space-area"></div>
	</main>
	
	<script>
	function startTaskApp() {
		let taskApp = new TaskApp({
			url: "main.php",
			dom: {
				areas: document.getElementById("areas"),
				areaSelector: document.getElementById("areaSelector"),
				tasks: document.getElementById("tasks"),
				commonSpaceArea: document.getElementById("common-space-area"),
				loadingicon: document.getElementById("loadingicon")
			}
		});
	}
	
	let libraryLoader = new LibraryLoader({
		directory: "js-base/",
		libraries: [
			"common.js",
			"tasks.js",
			"taskapp.js"
		]
	}).load().then((response) => {
		console.log(response);
		startTaskApp();
	}, function(error) {
		console.error("Failed!", error);
	});
	</script>
</body>

</html>
