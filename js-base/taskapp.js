class TaskApp {
	constructor(data) {
		data = data || {};
		this.url = data.url || "main.php";
		this.dom = data.dom || {};
		
		this.areas = [];
		
		this.getAreas();
		
		return this;
	}
	
	getAreas() {
		Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
		Common.loadData(this.url + "?list=area").then((response) => {
			Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
			try {
				let areas = JSON.parse(response);
				for(let key in areas) {
					if(key === "areas") {
						let newAreas = [];
						
						for(let areaKey in areas[key]) {
							let newArea = this.createArea(areas[key][areaKey]);
							newAreas.push(newArea);
						}
						
						for(let i = 0; i < this.areas.length; i++) {
							let exists = false;
							for(let j = 0; j < newAreas.length; j++) {
								if(this.areas[i].data.id === newAreas[j].data.id) {
									exists = newAreas[j];
								}
							}
							
							if(!exists) {
								this.areas[i].dom.parentNode.removeChild(this.areas[i].dom);
								this.areas.splice(i, 1);
							}
							else {
								let oldArea = this.areas[i].dom;
								let parent = oldArea.parentNode;
								
								this.areas[i] = exists;
								if(parent) {
									parent.replaceChild(exists.dom, oldArea);
								}
							}
						}
						
						for(let i = 0; i < newAreas.length; i++) {
							let exists = false;
							for(let j = 0; j < this.areas.length; j++) {
								if(newAreas[i].data.id === this.areas[j].data.id) {
									exists = true;
									break;
								}
							}
							if(!exists) {
								this.areas.push(newAreas[i]);
							}
						}
					}
				}				
				this.listAreas();
				this.areaSelector();
			}
			catch(e) {
				console.error("Parsing areas data failed", e.message);
			}
		}, function(error) {
			Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
			console.error("Failed!", error);
		});
	}
	
	readCookie(name) {
		return (name = new RegExp('(?:^|;\\s*)' + ('' + name).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') + '=([^;]*)').exec(document.cookie)) && name[1];
	}
	
	createArea(data) {
		data = data || {};
		
		let area = document.createElement("div");
		area.classList.add("area");
		
		let span = document.createElement("div");
		span.innerText = data.name || "";
		area.appendChild(span);
		
		let updateButton = document.createElement("button");
		updateButton.innerText = "update";
		area.appendChild(updateButton);
		
		let deleteButton = document.createElement("button");
		deleteButton.innerText = "delete";
		area.appendChild(deleteButton);
		
		/* Update wrapper */
		let updateWrapper = document.createElement("form");
		updateWrapper.classList.add("update-wrapper");
		let inputUpdate = document.createElement("input");
		updateWrapper.appendChild(inputUpdate);
		let saveButton = document.createElement("input");
		saveButton.type = "submit";
		saveButton.value = "save";
		updateWrapper.appendChild(saveButton);
		area.appendChild(updateWrapper);
		
		deleteButton.addEventListener("click", () => {
			if(confirm("Are you sure you want to delete this area and all tasks in it?")) {
				Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
				Common.pushPostData(this.url + "?delete=area&id=" + data.id).then((response) => {
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
					this.getAreas();
				}, function(error) {
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
					console.error("Failed!", error);
				});
			}
		});
		
		updateWrapper.addEventListener("submit", (event) => {
			event.preventDefault();
			if(inputUpdate.value) {
				Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
				Common.pushPostData(this.url + "?update=area&id=" + data.id + "&name=" + inputUpdate.value).then((response) => {
					this.getAreas();
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
				}, function(error) {
					console.error("Failed!", error);
					Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
				});
			}
		});
		
		updateButton.addEventListener("click", () => {
			updateButton.classList.toggle("active");
			updateWrapper.classList.toggle("visible");
		});
		
		return {
			dom: area,
			data: data
		};
	}
	
	listAreas() {
		if(this.dom.areas) {
			if(!this.areaList) {
				this.areaList = document.createElement("div");
				this.areaList.classList.add("area-list");
				
				let toggleList = document.createElement("button");
				toggleList.classList.add("toggle-list");
				toggleList.innerText = "Edit areas";
				this.dom.areas.appendChild(toggleList);
				
				toggleList.addEventListener("click", () => {
					toggleList.classList.toggle("active");
					this.areaList.classList.toggle("visible");
				});
				
				this.dom.areas.appendChild(this.areaList);
			}
			
			while(this.areaList.firstChild) {
				this.areaList.removeChild(this.areaList.firstChild);
			}
			
			for(let i = 0; i < this.areas.length; i++) {
				this.areaList.appendChild(this.areas[i].dom);
			}
			
			this.newAreaWrapper();
		}
	}
	
	newAreaWrapper() {
		if(this.areaList) {
			if(this.areaList.getElementsByClassName("new-area").length <= 0) {
				let newWrapper = document.createElement("form");
				newWrapper.classList.add("new-area");
				
				let inputNew = document.createElement("input");
				newWrapper.appendChild(inputNew);
				
				let newButton = document.createElement("input");
				newButton.type = "submit";
				newButton.value = "new";
				newWrapper.appendChild(newButton);
				
				this.areaList.appendChild(newWrapper);
				
				newWrapper.addEventListener("submit", (event) => {
					event.preventDefault();
					if(inputNew.value) {
						Common.changeLoaderVisibility(this.dom.loadingicon || null, "show");
						Common.pushPostData(this.url + "?new=area&name=" + inputNew.value).then((response) => {
							Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
							this.getAreas();
						}, function(error) {
							Common.changeLoaderVisibility(this.dom.loadingicon || null, "hide");
							console.error("Failed!", error);
						});
					}
				});
			}
		}
	}
	
	listTasks(id) {
		if(this.dom.tasks) {
			while(this.dom.tasks.firstChild) {
				this.dom.tasks.removeChild(this.dom.tasks.firstChild);
			}
			
			for(let i = 0; i < this.areas.length; i++) {
				if(this.areas[i].data.id === id) {
					let title = document.createElement("h2");
					title.innerText = this.areas[i].data.name;
					this.dom.tasks.appendChild(title);
					break;
				}
			}
			
			new Tasks({
				id: id,
				url: this.url,
				dom: {
					tasks: this.dom.tasks,
					loadingicon: this.dom.loadingicon
				}
			});
		}
	}
	
	areaSelector() {
		if(this.dom.areaSelector) {
			while(this.dom.areaSelector.firstChild) {
				this.dom.areaSelector.removeChild(this.dom.areaSelector.firstChild);
			}
			
			let areaSelector = document.createElement("select");
			
			let defaultOption = document.createElement("option");
			defaultOption.selected = true;
			defaultOption.disabled = true;
			defaultOption.innerText = " -- select an option -- ";
			areaSelector.appendChild(defaultOption);
			
			for(let i = 0; i < this.areas.length; i++) {
				let option = document.createElement("option");
				option.value = this.areas[i].data.id;
				option.innerText = this.areas[i].data.name;
				areaSelector.appendChild(option);
			}
			this.dom.areaSelector.appendChild(areaSelector);
			
			areaSelector.addEventListener("change", (event) => {
				let value = event.target.options[event.target.selectedIndex].value;
				document.cookie = "area=" + value;
				this.listTasks(Number(value));
			});
			
			let cookieValue = this.readCookie("area");
			if(cookieValue !== null) {
				for(let i = 0; i < areaSelector.options.length; i++) {
					if(areaSelector.options[i].value === cookieValue) {
						areaSelector.options[i].selected = true;
						break;
					}
				}
				
				this.listTasks(Number(cookieValue));
			}
		}
	}
}
