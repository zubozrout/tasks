<?php

class TaskData {
	public function __construct(stdClass $data) {
		if(!isset($data)) {
			return false;
		}
		
		foreach($data as $key => $value) {
			$this->{$key} = $value;
		}
		
		$this->checkValue("id", 0, "int");
		$this->checkValue("area_id", 0, "int");
		$this->checkValue("name", "NA", "string");
		$this->checkValue("description", "", "string");
		$this->checkValue("content", "NA", "string");
		$this->checkValue("extension", "", "string");
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getAreaId() {
		return $this->area_id;
	}
	
	private function checkValue($value, $default, $type) {
		if(!isset($this->{$value})) {
			$this->{$value} = $default;
			return false;
		}
		else {
			if($type === "int") {
				$this->{$value} = (int)$this->{$value};
			}
			else {
				$this->{$value} = (String)$this->{$value};
			}
		}
		return true;
	}
	
	public static function DBstructure() {
		$structure = [];
		$structure["id"] = "int(10) AUTO_INCREMENT";
		$structure["area_id"] = "int(4) NOT NULL";
		$structure["description"] = "varchar(255)";
		$structure["name"] = "varchar(255) NOT NULL";
		$structure["content"] = "varchar(1024) NOT NULL";
		$structure["extension"] = "varchar(4096)";
		$structure["PRIMARY KEY"] = "id";
		return $structure;
	}
}

?>
