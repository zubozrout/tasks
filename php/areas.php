<?php

class Areas {
	private $id = 0;
	private $name = null;
	
	private $dbOperations = null;
	private $taskTableName = "tasks";
	
	private $taskList = [];
	
	public function __construct(stdClass $data, DBOperations $dbOperations) {
		$this->id = (int)$data->id;
		$this->name = (String)$data->name;
		$this->dbOperations = $dbOperations;
		
		$this->loadDBTasks();
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getName() {
		return $this->name;
	}
	
	private function objectToTaskData($data) {
		if(!empty($data)) {
			return new TaskData($data);
		}
		return null;
	}
	
	private function loadDBTasks() {
        $response = $this->dbOperations->query("SELECT * FROM " . $this->taskTableName . " WHERE area_id = " . $this->id);
        if($response) {
			foreach($response as $task) {
				$task = new BasicTask($this->objectToTaskData($task), $this->dbOperations->database->getConnection());
				if(!$this->checkIfTaskIsRegistered($task)) {
					array_push($this->taskList, $task);
				}
			}
		}
    }
	
	public function registerTask(BasicTask $task) {
		if(!$this->checkIfTaskIsRegistered($task)) {
			array_push($this->taskList, $task);
			$this->saveTaskToDB($task);
			return $task;
		}
		return false;
	}
	
	public function updateTask($id, BasicTask $task) {
		$task->setId($id);
		$oldTask = $this->getTaskById($id);
		if($oldTask) {
			if(($key = array_search($oldTask, $this->taskList)) !== false) {
				$this->taskList[$key] = $task;
				$this->saveTaskToDB($task);
			}
			return false;
		}
		else {
			return $this->registerTask($task);
		}
		return false;
	}
	
	public function deleteTask($id) {
		$task = $this->getTaskById($id);
		if($task) {
			if(($key = array_search($task, $this->taskList)) !== false) {
				unset($this->taskList[$key]);
				$this->deleteTaskFromDB($task);
				return true;
			}
		}
		return false;
	}
	
	public function deleteAllTasks() {
		foreach($this->taskList as $task) {
			unset($this->taskList[$key]);
			$this->deleteTaskFromDB($task);
		}
		return true;
	}
	
	private function saveTaskToDB(BasicTask $task, $update = false) {
		$data = $task->getData();
		
		if($data) {
			$dataArray = (array)$data;
			if($update) {
				unset($dataArray["id"]);
			}
			
			$values = array_keys((array)$dataArray);
			$valuesString = implode(", ", $values);
			
			$names = [];
			foreach($dataArray as $key => $value) {
				array_push($names, ":" . $key);
			}
			$namesString = implode(", ", $names);
			
			return $this->dbOperations->insertOrUpdate("REPLACE INTO " . $this->taskTableName . " (${valuesString}) VALUES (${namesString})", $dataArray);
		}
		return false;
	}
	
	private function deleteTaskFromDB(BasicTask $task) {
		$id = (int)$task->getId();
		
		if($id) {
			return $this->dbOperations->insertOrUpdate("DELETE FROM " . $this->taskTableName . " WHERE id = (:id)", array("id" => $id));
		}
		return false;
	}
	
	private function getTaskById($id) {
		foreach($this->taskList as $task) {
			if($task->getId() === $id) {
				return $task;
			}
		}
		return false;
	}
	
	private function checkIfTaskIsRegistered(BasicTask $comparedTask) {
		if($this->getTaskById($comparedTask->getId())) {
			return true;
		}
		return false;
	}
	
	public function getTasks() {
		return $this->taskList;
	}
}

?>
