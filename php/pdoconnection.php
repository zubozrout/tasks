<?php

class DBConnection {
	protected $dbName;
	protected $connection;
	
	public function __construct($dbName) {
		$this->dbName = $dbName;
		$this->connection = $this->startConnection($this->dbName, "username", "password");
	}
	
	private function startConnection($dbname, $user, $pass) {
        $dsn = "mysql:host=localhost;dbname=$dbname;port=3336";
        $options = array(
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);
		
		try {
			return new PDO($dsn, $user, $pass, $options);
		}
		catch (PDOException $e) {
			throw new Exception("Could not connect to DB: " . $e->getMessage());
			die();
		}
	}
	
	public function __destruct() {
		$this->connection = null;
    }
    
    public function getConnection() {
		return $this->connection;
	}
    
    public function getDbName() {
		return $this->dbName;
	}
}

class DBDatabase extends DBConnection {    
	public function __construct($dbName) {
		if(!isset($dbName)) {
			return false;
		}
		
		parent::__construct($dbName);
	}
	
	public function getAllTableItems($table) {
		$pdoRequest = $this->connection->prepare("SELECT * FROM ?");
		$pdoRequest->execute(array($table));
		print_r($pdoRequest->fetchAll(PDO::FETCH_OBJ));
		return true;
	}
	
	public static function openDB($dbName) {
		$instance = new self($dbName);
		if($instance) {
			$instance->openDatabase();
		}
		return $instance;
	}
	
	public static function openAndCreateDB($dbName) {
		$instance = new self($dbName);
		if($instance) {
			$instance->createDatabase();
		}
		return $instance;
	}
	
	private function createDatabase() {
		$pdoRequest = $this->connection->prepare("CREATE DATABASE IF NOT EXISTS " . $this->dbName);
		if(!$pdoRequest->execute()) {
			throw new Exception($pdoRequest->errorInfo());
		}
	}
}

/* $db is either a string with a database name or an open database connection in the from of a DBDatabase object */
class DBOperations {  
	public function __construct() {
		$this->database = DBDatabase::openAndCreateDB("dbname");
	}
	
	public function createTable($tableName, $structure) {
		$pdoRequest = $this->database->getConnection()->prepare("CREATE TABLE IF NOT EXISTS $tableName ($structure)");
		if(!$pdoRequest->execute()) {
			throw new Exception($pdoRequest->errorInfo());
		}
	}
	
	// "SELECT id, firstname, lastname FROM MyGuests" - used for select
	public function query($sql) {
		$pdoRequest = $this->database->getConnection()->prepare($sql);
		if($pdoRequest->execute()) {
			return $pdoRequest->fetchAll(PDO::FETCH_OBJ);
		}
		else {
			throw new Exception($pdoRequest->errorInfo());
		}
	}
	
	// OBSOLETE, don't use - doesn't deal with SQL injection
	public function nonSelectQuery($sql) {
		$pdoRequest = $this->database->getConnection()->prepare($sql);
		if($pdoRequest->execute()) {
			if($pdoRequest->rowCount() > 0) {
				return $this->database->getConnection()->lastInsertId();
			}
			return null;
		}
		else {
			throw new Exception($pdoRequest->errorInfo());
		}
	}
	
	// Universal SQL query used to insert, update and delete
	public function insertOrUpdate($sql, Array $data) {
		$pdoRequest = $this->database->getConnection()->prepare($sql);
		if($pdoRequest->execute($data)) {
			if($pdoRequest->rowCount() > 0) {
				return $this->database->getConnection()->lastInsertId();
			}
			return null;
		}
		else {
			throw new Exception($pdoRequest->errorInfo());
		}
	}
	
	public function selectItems($items, $table) {
		return $this->select($items . " FROM " . $table);
	}
}
?>
