class Common {
	static loadData(url) {
		if(url) {
			return new Promise((resolve, reject) => {
				let xhr = new XMLHttpRequest();
				xhr.onreadystatechange = () => {
					if(xhr.readyState == 4) {
						if(xhr.status == 200) {
							resolve(xhr.response);
						}
						else {
							reject(Error(xhr.statusText));
						}
					}
				};
				xhr.onerror = function() {
					reject(Error("Network Error"));
				};
				xhr.open("GET", url, true);
				xhr.send();
			});
		}
		return false;
	}
	
	static pushPostData(url, data) {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange = () => {
				if(xhr.readyState == 4) {
					if(xhr.status == 200) {
						resolve(xhr.response);
					}
					else {
						reject(Error(xhr.statusText));
					}
				}
			};
			xhr.onerror = function() {
				reject(Error("Network Error"));
			};
			xhr.open("POST", url, true);
			xhr.send(data);
		});
	}
	
	static changeLoaderVisibility(loader, status) {
		if(loader && status) {
			if(status === "show") {
				loader.classList.add("visible");
			}
			else {
				loader.classList.remove("visible");
			}
		}
	}
}
